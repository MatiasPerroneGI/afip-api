<?php

return [
    'timeout' => 15, //segundos
    'produccion' => env('AFIP_MODO_PRODUCCION', false), // live o test
    'wsdl' => [
        'wsfe' => [
            'testing' => env('AFIP_WSDL_WSFE_TESTING', 'https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL'),
            'produccion' => env('AFIP_WSDL_WSFE_PRODUCCION', 'https://servicios1.afip.gov.ar/wsfev1/service.asmx?WSDL'),
        ],
        'wsaa' => [
            'testing' => env('AFIP_WSDL_WSAA_TESTING', 'https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL'),
            'produccion' => env('AFIP_WSDL_WSAA_PRODUCCION', 'https://wsaa.afip.gov.ar/ws/services/LoginCms?WSDL'),
        ],
    ],
    'opciones' => [
        'testing' => [
            'clave' => '' // Contraseña del archivo de clave privada
        ],
        'produccion' => [
            'clave' => '' // Contraseña del archivo de clave privada
        ],
    ]
];