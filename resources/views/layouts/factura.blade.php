<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <title>{{ $cliente->nombre }}</title>
    <link rel="stylesheet" href="./factura.css">
</head>

<body>
    <div class="canvas">
        <table class="encabezado">
            <tr>
                <td class="logo">
                    <img alt="logo Global Innovation Arg"
                        src="https://d33wubrfki0l68.cloudfront.net/3bb9be85d4c476b642f062d88f105aba0054b4f9/a4b8c/assets/img/logo.png">
                </td>
                <td class="letra">{{ $factura->letra }}</td>
                <td class="info-documento">
                    <p class="tipo">{{ $factura->tipo }}</p>
                    <table class="datos">
                        <tr><th>Número:</th><td>{{ $factura->puntodeventa }}-{{ $factura->numero }}</td></tr>
                        <tr><th>Fecha:</th><td>{{ $factura->fecha }}</td></tr>
                        <tr><th>CUIT:</th><td>{{ $cliente->cuitConFormato() }}</td></tr>
                        <tr><th>Ing. Brutos:</th><td>{{ $cliente->iibb }}</td></tr>
                        <tr><th>Inicio Act.:</th><td>{{ $cliente->inicio->format('d/m/Y') }}</td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="linea"></div>
        <table class="destinatario">
            <tbody>
                <tr>
                    <th>Sr. (es):</th>
                    <td>Un cliente random</td>
                    <th>CUIT</th>
                    <td>99-99999999-9</td>
                </tr>
                <tr>
                    <th>Domicilio</th>
                    <td>Calle Falsa 123 Piso: 100 Dpto: X, Ciudad Autónoma de Buenos Aires, Argentina </td>
                    <th>Cond. IVA</th>
                    <td>Responsable Inscripto</td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>
