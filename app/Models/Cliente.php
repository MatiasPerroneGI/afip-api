<?php

namespace GoNearby\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Cliente extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clientes';
    protected $guard = 'cliente';

    /**
     * Indicates the model field name.
     *
     * @var bool
     */
    // public $fieldId = 'cliente_id';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'id' => null,
        'cuit' => null,
        'nombre' => null,
        'responsabilidad' => null,
        'iibb' => null,
        'inicio' => null,
        'email' => null,
        'clave' => null,
        'secreto' => null,
        'habilitado' => true,
        'eliminado' => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email', 'cuit', 'responsabilidad', 'iibb', 'inicio', 'habilitado', 'secreto', 'clave'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = ['id', 'secreto'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // protected $appends = ['cuota'];

    /**
     * Obtiene todo el listado de cuotas asociadas
     */
    public function cuotas()
    {
        return $this->hasMany(Cuota::class);
    }

    /**
     * Obtiene el monto de la cuota restante
     */
    public function getCuota()
    {
        return $this->cuotas->sum('cantidad');
    }

    /**
     * Agrega el atributo cuota
     */
    public function getCuotaAttribute()
    {
        $cuota = null;
        if ($this->id) {
            $cuota = DB::select("SELECT `fnObtenerCuota`(?) cuota", [$this->id])[0]->cuota;
        }
        return $cuota;
    }

    /**
     * Agrega el atributo cuota
     */
    public function setSecretoAttribute($secreto)
    {
        $this->attributes['secreto'] = Hash::make($secreto);
        return $this;
    }

    /**
     * Agrega el atributo cuota
     */
    public function setNombreAttribute($nombre)
    {
        $this->attributes['nombre'] = ucwords(strtolower($nombre));
        return $this;
    }

    /**
     * Agrega el monto de la cuota
     */
    public function agregarCuota()
    {
        $this->appends = $this->appends + ['cuota'];
        return $this;
    }

    /**
     * Incluye el listado de cuotas asociadas
     */
    public function agregarDetalleCuotas()
    {
        return $this->agregarCuota()->with('cuotas');
    }

    public function cuitConFormato()
    {
        $cuit = $this->cuit ?: \str_repeat('0', 11);
        return preg_replace('([0-9]{2})([0-9]{8})([0-9])', '${1}-${2}-${3}', $cuit);
    }
}