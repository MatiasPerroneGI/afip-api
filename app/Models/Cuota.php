<?php

namespace GoNearby\Models;

class Cuota extends Model
{

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'fecha' => null,
        'cantidad' => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cantidad', 'cliente_id'
    ];

    /**
     * The attributes that are hidden.
     *
     * @var array
     */
    protected $hidden = [
        'cliente_id'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    /**
     * Get the post that owns the comment.
     */
    public function getCliente(): Cliente
    {
        return $this->cliente;
    }

    /**
     * Establecer el cliente al que pertenece la cuota
     */
    public function setCliente(Cliente $cliente = null): self
    {
        $this->cliente_id = $cliente ? $cliente->id : null;
        return $this;
    }
}
