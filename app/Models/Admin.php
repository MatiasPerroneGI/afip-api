<?php

namespace GoNearby\Models;


class Admin extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'admins';

    protected $guard = 'admin';

    /**
     * Indicates the model field name.
     *
     * @var bool
     */
    // public $fieldId = 'admin_id';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email',
    ];
}