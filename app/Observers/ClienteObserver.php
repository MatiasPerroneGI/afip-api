<?php

namespace GoNearby\Observers;

use GoNearby\Models\Cliente;
use GoNearby\Models\Cuota;

class ClienteObserver
{
    /**
     * Handle the cliente "created" event.
     *
     * @param  \GoNearby\Models\Cliente  $cliente
     * @return void
     */
    public function created(Cliente $cliente)
    {
        //
    }

    /**
     * Handle the cliente "updated" event.
     *
     * @param  \GoNearby\Models\Cliente  $cliente
     * @return void
     */
    public function updated(Cliente $cliente)
    {
        //
    }

    /**
     * Handle the cliente "deleted" event.
     *
     * @param  \GoNearby\Models\Cliente  $cliente
     * @return void
     */
    public function deleted(Cliente $cliente)
    {
        Cuota::where(['cliente_id' => $cliente->id])->delete();
    }

    /**
     * Handle the cliente "restored" event.
     *
     * @param  \GoNearby\Models\Cliente  $cliente
     * @return void
     */
    public function restored(Cliente $cliente)
    {
        //
    }

    /**
     * Handle the cliente "force deleted" event.
     *
     * @param  \GoNearby\Models\Cliente  $cliente
     * @return void
     */
    public function forceDeleted(Cliente $cliente)
    {
        Cuota::find($cliente->id)->forceDelete();
    }
}