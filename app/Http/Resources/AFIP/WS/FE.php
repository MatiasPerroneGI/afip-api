<?php


namespace GoNearby\Http\Resources\AFIP\WS;

use GoNearby\Http\Resources\AFIP\SoapClient;
use GoNearby\Http\Resources\AFIP\Token\Autorizacion;
use GoNearby\Http\Resources\ObjectPropertiesAccesor;
use Illuminate\Support\Facades\Cache;

class FE
{
    const CACHE_TTL = 86400;
    const METODOS = [
        // Tablas generales
        'FEParamGetTiposCbte' => ['rutaARetornar' => '/ResultGet/CbteTipo', 'envolvente' => null, 'cache' => true],
        'FEParamGetTiposConcepto' => ['rutaARetornar' => '/ResultGet/ConceptoTipo', 'envolvente' => null, 'cache' => true],
        'FEParamGetTiposDoc' => ['rutaARetornar' => '/ResultGet/DocTipo', 'envolvente' => null, 'cache' => true],
        'FEParamGetTiposIva' => ['rutaARetornar' => '/ResultGet/IvaTipo', 'envolvente' => null, 'cache' => true],
        'FEParamGetTiposMonedas' => ['rutaARetornar' => '/ResultGet/Moneda', 'envolvente' => null, 'cache' => true],
        'FEParamGetTiposOpcional' => ['rutaARetornar' => '/ResultGet/OpcionalTipo', 'envolvente' => null, 'cache' => true],
        'FEParamGetTiposTributos' => ['rutaARetornar' => '/ResultGet/TributoTipo', 'envolvente' => null, 'cache' => true],
        // Métodos
        'FEDummy' => ['rutaARetornar' => '/', 'envolvente' => null],
        'FECAESolicitar' => ['rutaARetornar' => '/', 'envolvente' => 'FeCAEReq'],
        'FEParamGetPtosVenta' => ['rutaARetornar' => '/ResultGet/PtoVenta', 'envolvente' => null],
        'FECompUltimoAutorizado' => ['rutaARetornar' => '/', 'envolvente' => null],
        'FECAEASinMovimientoInformar' => ['rutaARetornar' => '/', 'envolvente' => null],
        'FECAEASinMovimientoConsultar' => ['rutaARetornar' => '/ResultGet/FECAEASinMov', 'envolvente' => null],
        'FECAEASolicitar' => ['rutaARetornar' => '/ResultGet/FECAEAGet', 'envolvente' => 'FeCAEReq'],
        'FECAEAConsultar' => ['rutaARetornar' => '/ResultGet/FECAEAGet', 'envolvente' => null],
        'FECompConsultar' => ['rutaARetornar' => '/ResultGet', 'envolvente' => 'FeCompConsReq'],
        'FECAEARegInformativo' => ['rutaARetornar' => '/', 'envolvente' => 'FeCAEARegInfReq'],
    ];


    /**
     * Token
     *
     * @var GoNearby\Http\Resources\AFIP\Token
     */
    private $token;

    /**
     * CUIT
     *
     * @var string
     */
    private $cuit;

    /**
     * SoapClient ya configurado
     *
     * @var GoNearby\Http\Resources\AFIP\SoapClient
     */
    private $soap;

    public function __construct(string $cuit, string $wsdl, Autorizacion $token)
    {
        $this->cuit = $cuit;
        $this->token = $token;
        $this->soap = new SoapClient($wsdl);
    }

    public function __call(string $metodo, array $datos)
    {
        $this->_validarMetodo($metodo);
        $datos = $this->_agregarEstructura($metodo, $datos[0], $datos[1]);
        return $this->_obtenerResultado($metodo, $datos);
    }

    /**
     * Define las credenciales dependiendo del servicio
     *
     * @param string $metodo Función SOAP a realizar
     *
     * @return array Request parameters
     **/
    private function &_agregarEstructura($metodo, Autorizacion &$autorizacion = null, array &$datos = []): array
    {
        $params = [];
        if ($autorizacion) {
            if (self::METODOS[$metodo]['envolvente']) {
                $datos = [
                    self::METODOS[$metodo]['envolvente'] => $datos
                ];
            }
            $datos = $datos + [
                'Auth' => [
                    'Token' => $autorizacion->token,
                    'Sign'  => $autorizacion->sign,
                    'Cuit'  => $this->cuit
                ]
            ];
        }

        if (count($datos)) {
            $params = [$metodo => $datos];
        }

        return $params;
    }

    private function _validarMetodo(string $metodo): void
    {
        if (!array_key_exists($metodo, self::METODOS)) {
            abort(501, "El método $metodo es inexistente.");
        }
    }

    private function _obtenerResultado(string $metodo, array &$datos)
    {
        $cache = self::METODOS[$metodo]['cache'] ?? false;
        $callable = [$this->soap, $metodo];
        $fnSOAPCall = function () use ($callable, $datos) {
            return call_user_func_array($callable, $datos);
        };
        $respuesta = $cache ? Cache::remember("soap-fe-{$metodo}", self::CACHE_TTL, $fnSOAPCall) : $fnSOAPCall();
        $ruta = !property_exists($respuesta, 'Errors') ? $this->_ruta($metodo) : '/';
        return ObjectPropertiesAccesor::get($respuesta->{$metodo . 'Result'}, $ruta);
    }

    private function _ruta($metodo)
    {
        return (self::METODOS[$metodo]['rutaARetornar'] ?? false) ?: '';
    }
}