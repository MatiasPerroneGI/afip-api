<?php

namespace GoNearby\Http\Resources\AFIP\WS;

use DOMDocument;
use GoNearby\Http\Resources\AFIP\Exception\ErrorAlCrearLaFirmaSMime;
use GoNearby\Http\Resources\AFIP\Exception\ErrorAlSolicitarElToken;
use GoNearby\Http\Resources\AFIP\SoapClient;
use GoNearby\Http\Resources\AFIP\Token\Autorizacion;
use SoapFault;

class AA
{

    /**
     * URL del WSDL
     *
     * @var string
     */
    private $wsdl;

    /**
     * Nombre del WS
     *
     * @var string
     */
    private $ws;

    /**
     * Archivo del certificado
     *
     * @var string
     */
    private $certificado;

    /**
     * Archivo de clave privada
     *
     * @var array
     */
    private $clavePrivada;

    public function __construct($wsdl, $certificado, array $clavePrivada, string $ws = 'wsfe')
    {
        $this->wsdl = substr($wsdl, 0, 4) === 'http' ? $wsdl : storage_path("app/afip/$wsdl");
        $this->ws = $ws;
        $this->certificado = \file_get_contents($certificado);
        $this->clavePrivada = ["file://{$clavePrivada[0]}", $clavePrivada[1]];
    }

    private function _getXML()
    {
        $dom = new DOMDocument("1.0", "utf-8");
        $loginTicketRequest = $dom->createElement("loginTicketRequest");
        $header = $dom->createElement("header");
        $version = $dom->createAttribute("version");
        $service = $dom->createElement("service", $this->ws);
        $version->value = "1.0";
        $loginTicketRequest->appendChild($version);
        $loginTicketRequest->appendChild($header);
        $loginTicketRequest->appendChild($service);
        $header->appendChild($dom->createElement("uniqueId", date("U")));
        $header->appendChild($dom->createElement("generationTime", date("c", date("U") - 600)));
        $header->appendChild($dom->createElement("expirationTime", date("c", date("U") + 600)));
        $dom->appendChild($loginTicketRequest);
        return $dom->saveXML();
    }

    private function _sign()
    {
        $tmpXML = tempnam(sys_get_temp_dir(), 'go-nearby-afip-wsaa-xml-');
        $tmpSign = tempnam(sys_get_temp_dir(), 'go-nearby-afip-wsaa-sign-');

        file_put_contents($tmpXML, $this->_getXML());

        $ok = openssl_pkcs7_sign($tmpXML, $tmpSign, $this->certificado, $this->clavePrivada, [], !PKCS7_DETACHED);
        if (!$ok) {
            throw new ErrorAlCrearLaFirmaSMime('No pudo escribirse en el archivo destino', E_USER_ERROR);
        }

        $sign = explode("\n", file_get_contents($tmpSign));
        $sign = implode("\n", array_splice($sign, 5));
        unlink($tmpSign);
        unlink($tmpXML);
        return $sign;
    }

    private function _requestToken($sign)
    {
        try {
            $client = new SoapClient($this->wsdl);
            $resultado = (string) $client->loginCms(['in0' => $sign])->loginCmsReturn;
            return $resultado;
        } catch (SoapFault $error) {
            throw new ErrorAlSolicitarElToken($error->getMessage(), $error->getCode(), $error);
        }
    }

    /**
     * Obtiene la autorización en el servicio WSAA
     *
     * @return Autorizacion
     */
    public function token(): Autorizacion
    {
        return new Autorizacion($this->_requestToken($this->_sign()));
    }
}
