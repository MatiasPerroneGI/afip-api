<?php

namespace GoNearby\Http\Resources\AFIP\Token;

class Credenciales
{
    /**
     * Token
     *
     * @var string
     */
    private $token;

    /**
     * Firma
     *
     * @var string
     */
    private $sign;


    public function __construct(string $token, string $sign)
    {
        $this->token = $token;
        $this->sign = $sign;
    }

    public function __get(string $nombre): string
    {
        return $this->$nombre;
    }
}