<?php

namespace GoNearby\Http\Resources\AFIP\Token;

use DateTime;
use ReflectionClass;
use ReflectionObject;
use SimpleXMLElement;

class Autorizacion
{
    /**
     * Credenciales
     *
     * @var Credenciales
     */
    protected $credentials = null;

    /**
     * Encabezado del request a la AFIP
     *
     * @var Encabezado
     */
    protected $header = null;

    public function __construct(string $xml = null)
    {
        if (!empty($xml)) {
            $this->inicializar($xml);
        }
    }

    public function inicializar(string $xml)
    {
        $xml = new SimpleXMLElement($xml);
        $this->credentials = $this->_convertir(Credenciales::class, $xml->credentials);
        $this->header = $this->_convertir(Encabezado::class, $xml->header);
    }

    public function __get(string $nombre)
    {
        switch ($nombre) {
            case 'token':
                $valor = $this->credentials->token;
                break;
            case 'sign':
                $valor = $this->credentials->sign;
                break;
            case 'expiracion':
                $valor = $this->header->expirationTime;
                break;
            case 'vencimiento':
                $valor = new DateTime($this->header->expirationTime);
                break;
            case 'esVacio':
                $valor = ($this->credentials and $this->header) ? true : false;
        }
        return $valor;
    }

    public function token()
    {
        return $this->token;
    }

    public function sign()
    {
        return $this->sign;
    }

    public function expiracion()
    {
        return $this->expiracion;
    }

    /**
     * Class casting
     *
     * @param string|object $destinationClass
     * @param object $sourceObject
     * @return object
     */
    private function _convertir(string $destinationClass, object $sourceObject)
    {
        $destinationObject = (new ReflectionClass($destinationClass))->newInstanceWithoutConstructor();
        $destinationReflection = new ReflectionObject($destinationObject);
        $sourceReflection = new ReflectionObject($sourceObject);
        $sourceProperties = $sourceReflection->getProperties();
        foreach ($sourceProperties as $sourceProperty) {
            $name = $sourceProperty->getName();
            $value = (string) $sourceProperty->getValue($sourceObject);
            if ($destinationReflection->hasProperty($name)) {
                $propDest = $destinationReflection->getProperty($name);
                $propDest->setAccessible(true);
                $propDest->setValue($destinationObject, $value);
            } else {
                $destinationObject->$name = $value;
            }
        }
        return $destinationObject;
    }
}
