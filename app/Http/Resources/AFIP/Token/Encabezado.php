<?php

namespace GoNearby\Http\Resources\AFIP\Token;

use DateTime;

class Encabezado
{
    /**
     * Fecha de creación de las credenciales
     *
     * @var DateTime
     */
    protected $generationTime;

    /**
     * Fecha de vencimiento de las credenciales
     *
     * @var DateTime
     */
    protected $expirationTime;

    /**
     * Id de las credenciales
     *
     * @var integer
     */
    protected $uniqueId;

    /**
     * CUIT y nombre
     *
     * @var string
     */
    protected $destination;

    /**
     * Fuente
     *
     * @var string
     */
    protected $source;

    public function __construct(DateTime $generationTime, DateTime $expirationTime, int $uniqueId, string $destination, string $source)
    {
        $this->generationTime = $generationTime;
        $this->expirationTime = $expirationTime;
        $this->uniqueId = $uniqueId;
        $this->destination = $destination;
        $this->source = $source;
    }

    public function __get(string $nombre)
    {
        return $this->$nombre;
    }
}