<?php


namespace GoNearby\Http\Resources\AFIP;

use Illuminate\Support\Facades\Log;
use SimpleXMLElement;
use SoapClient as PHPSoapClient;

class SoapClient extends PHPSoapClient
{

    public function __construct($wsdl, array $opciones = [])
    {
        $produccion = config('afip.produccion');
        ini_set('soap.wsdl_cache_enabled', true);
        ini_set('soap.wsdl_cache_ttl', 86400); // 1 día

        $opciones = [
            'soap_version' => \SOAP_1_2,
            'user_agent' => 'Go nearby API Client',
            'cache_wsdl' => $produccion ? WSDL_CACHE_BOTH : WSDL_CACHE_NONE,
            'compression' => SOAP_COMPRESSION_GZIP,
            'trace' => true,
            'exceptions' => true,
            'connection_timeout' => config('afip.timeout', 10),
        ];
        $modo = $produccion ? 'produccion' : 'testing';
        $wsdl = substr($wsdl, 0, 4) === 'http' ? $wsdl : storage_path("app/afip/$modo/$wsdl");
        return parent::__construct($wsdl, $opciones);
    }

    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $level = 'debug';
        $xml = str_repeat('`', 3) . PHP_EOL . (new SimpleXMLElement($request))->asXML() . PHP_EOL . str_repeat('`', 3);
        Log::channel('slack')->$level("SOAP -> Request de $action ($location):\n$xml");

        $respuesta = parent::__doRequest($request, $location, $action, $version, $one_way);
        try {
            $xml = str_repeat('`', 3) . PHP_EOL . (new SimpleXMLElement($respuesta))->asXML() . PHP_EOL . str_repeat('`', 3);
        } catch (\Exception $e) {
            $xml = str_repeat('`', 3) . PHP_EOL . $respuesta . PHP_EOL . str_repeat('`', 3);
            $level = 'critical';
        }
        Log::channel('slack')->$level("SOAP -> Respuesta de $action:\n$xml");
        return $respuesta;
    }
}