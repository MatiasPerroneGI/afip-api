<?php

namespace GoNearby\Http\Resources\AFIP;

use GoNearby\Http\Resources\AFIP\Token\Autorizacion;
use GoNearby\Http\Resources\AFIP\Token\Credenciales;
use GoNearby\Http\Resources\AFIP\Token\Encabezado;
use GoNearby\Http\Resources\AFIP\WS\AA as WSAA;
use Illuminate\Support\Facades\Cache;

class Cliente
{
    /**
     * Instancia de Afip
     *
     * @var \GoNearby\Http\Resources\AFIP\SoapClient
     */
    public $soap = null;

    /**
     * Métodos que no llevan autenticación
     *
     * @var string[]
     */
    public $noAutenticar = [
        'FEDummy'
    ];

    /**
     * Modo de funcionamiento: producción o testing
     *
     * @var string
     */
    public $modo = 'testing';

    /**
     * Web service actual
     *
     * @var string
     */
    public $ws = null;

    /**
     * Web service autenticacion
     *
     * @var string
     */
    public $aa = null;

    /**
     * Configuración de la instancia
     *
     * @var array
     */
    private $config = [];

    public function __construct(string $ws, string $cuit)
    {
        $this->ws = $ws;
        $this->modo = config('afip.produccion') ? 'produccion' : 'testing';
        $this->config = [
            'dir' => storage_path("app/afip/{$this->modo}"),
            'certificado' => "{$this->ws}.crt",
            'clave' => ["{$this->ws}.key", config("afip.opciones.{$this->modo}.clave", '')],
            'cuit' => $cuit
        ];
    }

    public function rutaWSAA()
    {
        return config("afip.wsdl.wsaa.{$this->modo}", false) ?: abort(500, "La ruta para el WSDL de wsaa no está definida en la configuración.");
    }

    public function rutaCertificado()
    {
        return $this->config['dir'] . DIRECTORY_SEPARATOR . $this->config['certificado'];
    }

    public function rutaWSDL()
    {
        $wsdl = config("afip.wsdl.{$this->ws}.{$this->modo}", false);
        abort_unless($wsdl, 500, "La ruta para el WSDL de {$this->ws} no está definida en la configuración.");
        return $wsdl;
    }

    public function clavePrivada()
    {
        return [
            $this->config['dir'] . DIRECTORY_SEPARATOR . $this->config['clave'][0],
            $this->config['clave'][1]
        ];
    }

    public function __get($config)
    {
        return $this->config[$config] ?? null;
    }

    public function token()
    {
        $tokenNombre = "afip-token-{$this->cuit}-{$this->modo}";
        if (!Cache::has($tokenNombre)) {
            $token = $this->aa->token();
            if ($token) {
                Cache::put($tokenNombre, serialize($token), $token->vencimiento);
            }
        } else {
            $token = unserialize(Cache::get($tokenNombre), [
                'allowed_classes' => [Autorizacion::class, Credenciales::class, Encabezado::class]
            ]);
        }

        return $token;
    }

    private function _inicializarWSAA()
    {
        if (!$this->aa) {
            $this->aa = new WSAA($this->rutaWSAA(), $this->rutaCertificado(), $this->clavePrivada());
        }
    }

    private function _inicializarWS(string $cuit)
    {
        if (is_string($this->ws)) {
            $ns = __NAMESPACE__ . '\\WS\\';
            $WebService = $ns . strtoupper(strtolower(substr($this->ws, 0, 2)) === 'ws' ? substr($this->ws, 2) : $this->ws);
            $this->ws = new $WebService($cuit, $this->rutaWSDL(), $this->token());
        }
    }

    public function __call(string $metodo, array $argumentos)
    {
        $this->_inicializarWSAA();
        $this->_inicializarWS($this->cuit);
        $token = in_array($metodo, $this->noAutenticar) ? null : $this->token();
        return call_user_func_array([$this->ws, $metodo], [$token, array_shift($argumentos)]);
    }
}