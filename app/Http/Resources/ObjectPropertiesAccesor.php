<?php

namespace GoNearby\Http\Resources;

class ObjectPropertiesAccesor
{
    /**
     * Access to the properties sub
     *
     * @var \object $obj Object to be accessed
     * @var array|string Path to the properties and subproperties.
     */
    public static function &get(object &$obj, $path, string $delimiter = '/')
    {
        $r = null;
        if (is_string($path)) {
            $path = array_filter(explode($delimiter, $path), function ($value) {
                return trim($value) !== '';
            });
        }
        if (count($path)) {
            $property = array_shift($path);
            if ($property) {
                $r = property_exists($obj, $property) ?
                    count($path) ? self::get($obj->$property, $path) : $obj->$property
                    : null;
            }
        } else {
            $r = &$obj;
        }
        return $r;
    }
}
