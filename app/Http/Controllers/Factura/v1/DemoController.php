<?php

namespace GoNearby\Http\Controllers\Factura\v1;

use DateTime;
use GoNearby\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use TCPDF;

class DemoController extends Controller
{
    public function factura(Request $request)
    {
        $cliente = new Cliente([
            'nombre' => 'Global Innovation',
            'cuit' => '30716375966',
            'direccion' => 'Av Cramer 2447 14° B, Capital Federal, Ciudad Autónoma de Buenos Aires, Argentina',
            'responsabilidad' => 'Responsable Inscripto',
        ]);
        $pdf = new TCPDF();
        $pdf->SetCreator('Go Nearby');
        $pdf->SetAuthor($cliente->nombre);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->SetFont('arial');
        $pdf->AddPage();
        // $pdf->writeHTML(file_get_contents('~/resources/html/index.html'));
        $datos = [
            'cliente' => &$cliente,
            'factura' => (object) [
                'letra' => 'A',
                'tipo' => 'Factura',
                'puntodeventa' => '0000',
                'numero' => '00000000',
                'fecha' => new DateTime(),
            ]
        ];
        $pdf->writeHTML(view('layaouts/factura', $datos));

        return Response::make($pdf->Output(null, 'S'), 200, $headers);
        return file_get_contents('~/resources/html/index.html');
    }
}