<?php

namespace GoNearby\Http\Controllers\Factura\v1;

use Illuminate\Http\Request;
use GoNearby\Http\Controllers\Controller as MainController;

class Controller extends MainController
{
    //
    public function demo($cuit, Request $request)
    {
        return (strlen($cuit) == 11 and is_numeric($cuit)) ? 'Factura: Hola CUIT ' . $cuit : 'El CUIT no es válido';
    }
}