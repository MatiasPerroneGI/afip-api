<?php

namespace GoNearby\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

abstract class AuthController extends Controller
{

    /**
     * Get a JWT via given credencials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $modelClass = '\\' . \config('auth.providers.' . \config("auth.guards.{$this->guard}.provider") . '.model');
        $auth = auth($this->guard)->claims(['prv' => $modelClass]);
        $credenciales = $request->only(['clave', 'secreto']);
        $token = $auth->attempt(['clave' => $credenciales['clave'], 'password' => $credenciales['secreto']]);
        if (!$token) {
            throw new UnauthorizedHttpException('El usuario y/o la clave son inválidos');
        }

        return $this->respondWithToken($token);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        Auth::guard($this->guard)->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->me());
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(Auth::guard($this->guard)->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard($this->guard)->factory()->getTTL() * 60
        ]);
    }
}