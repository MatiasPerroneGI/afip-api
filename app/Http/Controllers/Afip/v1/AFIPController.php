<?php

namespace GoNearby\Http\Controllers\Afip\v1;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use GoNearby\Http\Resources\AFIP\Cliente as AFIP;
use GoNearby\Models\Cuota;
use Illuminate\Support\Facades\Log;

class AFIPController extends Controller
{
    /**
     * @var GoNearby\Http\Resources\AFIP\Cliente
     */

    private $wsfe;

    public function __invoke(string $metodo, Request $request, ...$argumentos)
    {
        ini_set('default_socket_timeout', 10);
        if (!method_exists($this, $metodo)) {
            try {
                abort_unless($this->_tieneCuota(), 429, 'La cuota ha alcanzado el límite');
                $this->wsfe = new AFIP('wsfe', $this->usuario()['cuit']);
                $respuesta = $this->wsfe->$metodo($request->all());
                $hayErrores = is_object($respuesta) ?
                    property_exists($respuesta, 'Errors') : array_key_exists('Errors', $respuesta);
                $codigo = $hayErrores ? 502 : 200;
                $this->_descontarCuota();
                return new JsonResponse($respuesta, $codigo);
            } catch (\SoapFault $error) {
                $msg = "AFIP: " . $error->getMessage();
                Log::critical($msg, [$error->getTraceAsString()]);
                abort(502, $msg);
            }
        } else {
            return call_user_func_array([$this, $metodo], [$request] + $argumentos);
        }
    }

    private function _descontarCuota()
    {
        Cuota::create([
            'cantidad' => -1,
            'cliente_id' =>  $this->usuario()->id
        ]);
    }

    private function _tieneCuota()
    {
        return ($this->usuario()->getCuotaAttribute() > 0);
    }
}