<?php

namespace GoNearby\Http\Controllers\Admin\v1;

use Illuminate\Http\Request;
use GoNearby\Http\Controllers\AdminController;
use GoNearby\Models\Cliente;
use GoNearby\Models\Cuota;

class CuotaController extends AdminController
{
    public function obtener(Request $request, Cliente $cliente)
    {
        return ["cantidad" => $cliente->getCuota()];
    }

    public function agregar(Request $request, Cliente $cliente)
    {
        $cantidad = $request->json('cantidad');
        $delCliente = $cliente->getCuota();
        $nuevaCantidad = $cantidad + $delCliente;

        // Si el resultado es negativo => asignar una cantidad menor para que quede en cero (0)
        if ($nuevaCantidad < 0) {
            $cantidad = $cantidad + abs($nuevaCantidad);
        }

        if ($cantidad) {
            Cuota::create([
                'cantidad' => $cantidad,
                'cliente_id' => $cliente->id
            ]);
        }
        return $this->obtener($request, $cliente->refresh());
    }
}