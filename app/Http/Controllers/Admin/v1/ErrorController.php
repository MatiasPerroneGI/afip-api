<?php

namespace GoNearby\Http\Controllers\Admin\v1;

use Illuminate\Http\Request;
use GoNearby\Http\Controllers\AdminController;

class ErrorController extends AdminController
{
    public function http404(Request $request)
    {
        return response()->json([
            'error' => 'No existe la ruta indicada'
        ], 404);
    }
}