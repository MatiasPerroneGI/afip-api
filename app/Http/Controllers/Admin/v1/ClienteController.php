<?php

namespace GoNearby\Http\Controllers\Admin\v1;

use Illuminate\Http\Request;
use GoNearby\Http\Controllers\AdminController;
use GoNearby\Models\Cliente;
use GoNearby\Http\Resources\Cliente as ClienteResourse;

class ClienteController extends AdminController
{
    public function listar(Request $request)
    {
        $eliminados = filter_var($request->get('eliminados'), FILTER_VALIDATE_BOOLEAN);
        $clientes = ($eliminados ? Cliente::withTrashed()->get() : Cliente::all());
        return $clientes;
    }

    public function obtener(Request $request, Cliente $cliente)
    {
        return new ClienteResourse($cliente->agregarCuota());
    }

    public function guardar(Request $request, string $cuit)
    {
        $cliente = Cliente::withTrashed()->where(['cuit' => $cuit])->first();
        return $cliente ? $this->_actualizar($request, $cliente) : $this->_crear($request, $cuit);
    }

    private function _actualizar(Request $request, Cliente &$cliente)
    {
        $request->validate([
            'email' => 'email|max:100',
            'nombre' => 'max:100',
            'secreto' => 'min:6'
        ]);
        $cliente
            ->fill($request->toArray())
            ->save();
        if ($cliente->trashed()) {
            $cliente->restore();
        }
        $cliente = $cliente ? $cliente->refresh() : false;
        return $cliente ? new ClienteResourse($cliente) : abort(500, "No fue posible actualizar el cliente");
    }

    private function _crear(Request $request, string $cuit)
    {
        $request->validate([
            'email' => 'required|unique:clientes|email|max:100',
            'nombre' => 'required|max:100',
            'secreto' => 'required',
            'clave' => 'max:0'
        ]);
        $datos = $request->toArray();
        $datos['clave'] = $datos['clave'] ?? md5($cuit);
        $cliente = Cliente::create($datos + ['cuit' => $cuit]);
        return $cliente ? new ClienteResourse($cliente) : abort(500, "No fue posible crear el cliente");
    }

    public function eliminar(Request $request, Cliente $cliente)
    {
        $cliente = $cliente->delete() ? $cliente->refresh() : false;
        return $cliente ? new ClienteResourse($cliente) : abort(500, "No fue posible eliminar el cliente");
    }
}