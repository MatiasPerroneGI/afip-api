<?php

namespace GoNearby\Http\Controllers\Admin\v1;

use GoNearby\Http\Controllers\AuthController as MainController;

class AuthController extends MainController
{
    // Guard to use
    protected $guard = 'admin';
}