<?php

namespace GoNearby\Providers;

use GoNearby\Models\Cliente;
use GoNearby\Observers\ClienteObserver;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Laravel Telescope
        // if ($this->app->isLocal()) {
        //     $this->app->register(TelescopeServiceProvider::class);
        // }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Resource::withoutWrapping();
        Cliente::observe(ClienteObserver::class);

        Validator::extend('cuit', 'GlobalInnovation\Validation\Rules\CUIT@validate');
        Validator::replacer('cuit', "El CUIT es inválido.");
    }
}