<?php

namespace GoNearby\Providers;

use GoNearby\Models\Cliente;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'GoNearby\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::pattern('cuit', '[0-9]{11}');
        Route::pattern('cliente', '[0-9]{11}');
        Route::pattern('cliente_con_eliminados', '[0-9]{11}');
        Route::pattern('id', '[0-9]+');
        Route::bind('cliente', function ($cuit) {
            return Cliente::where('cuit', $cuit)->first() ?? abort(404, 'El cliente no existe o fue eliminado');
        });
        Route::bind('cliente_con_eliminados', function ($cuit) {
            return Cliente::withTrashed()->where('cuit', $cuit)->first() ?? abort(404, 'El cliente no existe en la base de datos');
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $dominioPrincipal = '.' . env('APP_DOMINIO');
        $dominioAdmin = env('APP_SUBDOMINIO_ADMIN') . $dominioPrincipal;
        $dominioAfip = env('APP_SUBDOMINIO_AFIP') . $dominioPrincipal;
        $dominioFactura = env('APP_SUBDOMINIO_FACTURA') . $dominioPrincipal;

        $this->mapAdminRoutes($dominioAdmin);
        $this->mapAfipRoutes($dominioAfip);
        $this->mapFacturaRoutes($dominioFactura);
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAdminRoutes(string $dominio)
    {
        Route::domain($dominio)
            ->middleware('apiadmin')
            ->namespace($this->namespace . "\\Admin\\v1")
            ->group(base_path('routes/admin.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAfipRoutes(string $dominio)
    {
        Route::domain($dominio)
            ->middleware('api')
            ->namespace($this->namespace . "\\Afip\\v1")
            ->group(base_path('routes/afip.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapFacturaRoutes(string $dominio)
    {
        Route::domain($dominio)
            ->middleware('api')
            ->namespace($this->namespace . "\\Factura\\v1")
            ->group(base_path('routes/factura.php'));
    }
}