<?php

namespace GoNearby\Providers;

use GoNearby\Providers\Auth\JWTGuard as GoNearbyJWTGuard;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Tymon\JWTAuth\Facades\JWTProvider;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'GoNearby\Model' => 'GoNearby\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->app['auth']->extend('jwt', function ($app, $name, array $config) {
            $dominio = explode(':', $_SERVER['HTTP_HOST'] ?? ($_SERVER['SERVER_NAME'] ?? ''))[0];
            $subdominio = explode('.', $dominio, 2)[0];
            $esAdmin = $subdominio === 'admin';
            $provider = $esAdmin ? 'admins' : $config['provider'];

            $guard = new JWTGuard(
                $app['tymon.jwt'],
                $app['auth']->createUserProvider($provider),
                $app['request']
            );

            $app->refresh('request', $guard, 'setRequest');

            return $guard;
        });
    }
}