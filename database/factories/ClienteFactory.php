<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use GlobalInnovation\Validation\Rules\CUIT;
use GoNearby\Models\Cliente;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Cliente::class, function (Faker $faker) {
    // CUIT
    $inicioCUIT = array_rand(['20', '23', '24', '25', '26', '27', '30', '33']);
    $cuit = $inicioCUIT . Str::random(8);
    $cuit = $cuit . CUIT::verificationDigit($cuit);
    //Email
    $email = $faker->unique()->safeEmail;
    return [
        'cuit' => $cuit,
        'nombre' => $faker->name,
        'email' => $email,
        'clave' => Str::random(30),
        'secreto' => Hash::make($email), // password
    ];
});