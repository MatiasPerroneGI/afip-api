<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use GoNearby\Models\Admin;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Admin::class, function (Faker $faker) {
    $email = $faker->unique()->safeEmail;
    return [
        'nombre' => $faker->name,
        'email' => $email,
        'clave' => Str::random(30),
        'secreto' => Hash::make($email), // password
    ];
});