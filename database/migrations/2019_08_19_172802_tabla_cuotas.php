<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use GoNearby\Models\Model;
use Illuminate\Database\Schema\Grammars\MySqlGrammar;
use Illuminate\Support\Facades\DB;

class TablaCuotas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::create('cuotas', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('cliente_id')->unsigned();
                $table->bigInteger('cantidad');
                $table->timestamp(Model::CREATED_AT)->nullable();
                $table->timestamp(Model::UPDATED_AT)->nullable();
                $table->softDeletes(Model::DELETED_AT);
                $table->dateTime('fecha')->virtualAs(Model::CREATED_AT);
                $table->foreign('cliente_id', 'FK_cuotas_clientes')
                    ->references('id')->on('clientes');
            });
        } catch (\Exception $e) {
            $this->down();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuotas');
    }
}
