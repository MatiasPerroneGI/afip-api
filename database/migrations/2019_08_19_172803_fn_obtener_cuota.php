<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use GoNearby\Models\Model;

class FnObtenerCuota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE FUNCTION `fnObtenerCuota`(
                        `ClienteID` INT
                )
                RETURNS INT(20)
                READS SQL DATA
                COMMENT 'Obtiene la cuota de un cliente'
                RETURN (SELECT IFNULL(SUM(cantidad), 0) cuota FROM cuotas WHERE cliente_id = `ClienteID`);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP FUNCTION `fnObtenerCuota`");
    }
}