<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use GoNearby\Models\Model;

class TablaClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cuit', 11)->unique('IX_clientes_cuit');
            $table->string('nombre', 100);
            $table->string('email', 100)->unique('IX_clientes_email');
            $table->string('clave', 100);
            $table->string('secreto', 100);
            $table->boolean('habilitado')->default(1);
            $table->timestamp(Model::CREATED_AT, 0)->nullable();
            $table->timestamp(Model::UPDATED_AT, 0)->nullable();
            $table->softDeletes(Model::DELETED_AT);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}