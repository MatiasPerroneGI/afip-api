<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use GoNearby\Models\Model;
use Illuminate\Support\Facades\DB;

class TablaAdmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->string('email', 300)->unique('IX_administradores_email');
            $table->string('clave', 100);
            $table->string('secreto', 100);
            $table->timestamp(Model::CREATED_AT, 0)->nullable();
            $table->timestamp(Model::UPDATED_AT, 0)->nullable();
            $table->softDeletes(Model::DELETED_AT);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}