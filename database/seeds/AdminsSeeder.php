<?php

use GoNearby\Models\Admin;
use Illuminate\Database\Seeder;

class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::query()->truncate(); // truncate user table each time of seeders run
        // factory(Admin::class, 5)->create();
        Admin::create([
            'nombre' => 'Administrador',
            'email' => 'admin@admin.com',
            'clave' => 'admin',
            'secreto' => Hash::make('admin'),
        ]);
    }
}