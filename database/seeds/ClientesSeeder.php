<?php

use GoNearby\Models\Cliente;
use GoNearby\Models\Cuota;
use Illuminate\Database\Seeder;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cuota::query()->truncate();
        Cliente::query()->forceDelete(); // truncate user table each time of seeders run
        // factory(Cliente::class, 5)->create()->each(function (Cliente $cliente) {
        //     $cliente->cuotas()->save(factory(Cuota::class)->make());
        // });
        Cliente::create([
            'cuit' => '20266256362',
            'nombre' => 'Matias Perrone',
            'email' => 'matias.perrone@gmail.com',
            'clave' => hash('sha256', '20266256362'),
            'secreto' => Hash::make('20266256362'),
        ]);
    }
}