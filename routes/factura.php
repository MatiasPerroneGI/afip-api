<?php

Route::group(['prefix' => 'auth'], function () {
    Route::post('/', 'AuthController@login');
    Route::post('/logout', 'AuthController@logout');
    Route::post('/refresh', 'AuthController@refresh');
});

Route::group(['prefix' => 'cliente', 'middleware' => 'jwt.auth'], function () {
    Route::get('{cuit}', 'Controller@demo');
});

Route::get('/demo', 'DemoController@factura');