<?php

Route::group(['prefix' => 'auth'], function () {
    Route::post('/', 'AuthController@login')->name('login');
    Route::post('/logout', 'AuthController@logout');
    Route::post('/refresh', 'AuthController@refresh');
});

Route::group(['prefix' => 'gestion', 'middleware' => ['jwt.auth']], function () {
    Route::get('clientes', 'ClienteController@listar');
    Route::get('cliente/{cliente}', 'ClienteController@obtener');
    Route::post('cliente/{cuit}', 'ClienteController@guardar');
    Route::delete('cliente/{cliente}', 'ClienteController@eliminar');

    Route::get('cuota/{cliente}', 'CuotaController@obtener');
    Route::post('cuota/{cliente}', 'CuotaController@agregar');

    Route::fallback('ErrorController@http404');
});