<?php

Route::group(['prefix' => 'auth', 'middleware' => ['api']], function () {
    Route::post('/', 'AuthController@login');
    Route::post('/logout', 'AuthController@logout');
    Route::post('/refresh', 'AuthController@refresh');
});

Route::group(['prefix' => 'cliente', 'middleware' => 'jwt.auth'], function () {
    Route::get('/', 'Controller@demo');
});

Route::group(['prefix' => 'gestion', 'middleware' => 'jwt.auth'], function () {
    Route::any('/cuota', 'CuotaController@demo');
    Route::any('/cae', 'CaeController@demo');
});

Route::middleware('jwt.auth')->post('/{metodo}', 'AFIPController');