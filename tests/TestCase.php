<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected static function getAdminDomainHeaders()
    {
        return [
            'SERVER_NAME' => 'admin.go-nearby.desa',
            'HTTP_HOST' => 'admin.go-nearby.desa',
        ];
    }

    protected static function getAfipDomainHeaders()
    {
        return [
            'SERVER_NAME' => 'afip.go-nearby.desa',
            'HTTP_HOST' => 'afip.go-nearby.desa',
        ];
    }

    protected static function getFacturaDomainHeaders()
    {
        return [
            'SERVER_NAME' => 'factura.go-nearby.desa',
            'HTTP_HOST' => 'factura.go-nearby.desa',
        ];
    }
}