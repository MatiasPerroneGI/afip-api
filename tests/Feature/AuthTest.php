<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUnauthorized()
    {
        $response = $this->post('http://admin.go-nearby.desa/auth');

        // var_dump($response->json());

        $response->assertUnauthorized();
    }

    // /**
    //  * A basic test example.
    //  *
    //  * @return void
    //  */
    // public function testAuthorized()
    // {
    //     $response = $this->post('http://admin.go-nearby.desa/auth', ['clave' => ]);

    //     // var_dump($response->json());

    //     $response->assertUnauthorized();
    // }
}